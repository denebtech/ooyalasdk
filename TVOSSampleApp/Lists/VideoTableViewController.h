//
//  VideoTableViewController.h
//  TVOSSampleApp
//
//  Created by Vasyl Savka on 28/10/16.
//  Copyright © 2016 Ooyala. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoTableViewController : UITableViewController

@end
