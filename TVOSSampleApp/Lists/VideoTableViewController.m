//
//  VideoTableViewController.m
//  TVOSSampleApp
//
//  Created by Vasyl Savka on 28/10/16.
//  Copyright © 2016 Ooyala. All rights reserved.
//

#import "VideoTableViewController.h"
#import "ChildPlayerViewController.h"
#import "PlayerSelectionOption.h"
#import "GAUtils.h"

@interface VideoTableViewController ()

@property (nonatomic, strong) NSMutableArray *options; // of Video.h

@end

@implementation VideoTableViewController

- (void)viewDidLoad
{
  [super viewDidLoad];
    
  [self populateOptions];
    
  [[GATracker sharedInstance] screenView:@"Menu" :nil];
}

- (NSMutableArray *)options
{
  if (!_options) {
    _options = [NSMutableArray array];
  }
  return _options;
}

- (void)populateOptions
{
   //Read the comments in ChildPlayerViewController.h to know what this example is for
  [self.options addObject:[[PlayerSelectionOption alloc] initWithTitle:@"GA Test"
                                                             embedCode:@"k3dzcxNzE6cu91PKGnwG1QlhGeAEwsK8"
                                                                 pcode:@"F0ZWMyOnUqe6oU8JcI8YyWUZvLz-"
                                                                domain:@"http://www.ooyala.com"
                                                             segueName:@"childSegue"]];

  [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.options.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OptionCell" forIndexPath:indexPath];
  
  PlayerSelectionOption *option = self.options[indexPath.row];
  cell.textLabel.text = option.title;
  
  return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  PlayerSelectionOption *option = self.options[indexPath.row];
  [self performSegueWithIdentifier:option.segueName sender:[self.tableView cellForRowAtIndexPath:indexPath]];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
  if ([segue.identifier isEqualToString:@"childSegue"]){
    ChildPlayerViewController *destinationVC = segue.destinationViewController;
    destinationVC.option = self.options[indexPath.row];
  }
}

@end
