//
//  ChildPlayerViewController.m
//  TVOSSampleApp
//
//  Created by Vasyl Savka on 28/10/16.
//  Copyright © 2016 Ooyala. All rights reserved.
//

#import <OoyalaTVSkinSDK/OOOoyalaTVPlayerViewController.h>
#import "ChildPlayerViewController.h"
#import "PlayerSelectionOption.h"
#import "GAUtils.h"

@interface ChildPlayerViewController ()

@property (weak, nonatomic) IBOutlet UIView *playerView;


@end

@implementation ChildPlayerViewController

- (void)viewDidLoad {
  [super viewDidLoad];
    
  [[GATracker sharedInstance] screenView:@"Player" :nil];

  self.pcode = self.option.pcode;
  self.playerDomain = self.option.domain;
  
    OOOoyalaTVPlayerViewController* playerController = [OoyalaUtils createPlayer:self.playerDomain :self.pcode :self.option.embedCode];
  
  [self addChildViewController:playerController];
  //self.ooyalaPlayerViewController.view.frame = self.playerView.bounds;
  [self.playerView addSubview:playerController.view];
  [playerController.view setFrame:self.playerView.bounds];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [OoyalaUtils shutdownPlayer];
}

@end
