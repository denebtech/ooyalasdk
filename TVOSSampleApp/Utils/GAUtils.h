//
//  GAUtils.h
//  OoyalaGA
//
//  Created by Vasyl Savka on 28/10/16.
//  Copyright © 2016 Ooyala. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OoyalaTVSkinSDK/OOOoyalaTVPlayerViewController.h>
#import <OoyalaSDK/OOOoyalaPlayer.h>
#import <OoyalaSDK/OOPlayerDomain.h>

#ifndef GAUtils_h
#define GAUtils_h

@interface OoyalaUtils: NSObject

+(OOOoyalaTVPlayerViewController *)createPlayer:(NSString *)domain :(NSString *)pcode :(NSString *)embedCode;
+(void)shutdownPlayer;

@end

@interface GATracker : NSObject

//@property NSString *embedCode;
//@property NSString *domain;
@property OOOoyalaPlayer *player;
//@property OOOoyalaTVPlayerViewController *playerController;

@property (nonatomic, copy) NSString *tid;
@property (nonatomic, copy) NSString *MPVersion;
@property (nonatomic, copy) NSString *appName;
@property (nonatomic, copy) NSString *appVersion;
@property (nonatomic, copy) NSString *cid;
@property (nonatomic, copy) NSString *ua;
@property (nonatomic, copy) NSString *ul;
@property (nonatomic, copy) NSString *category;
//@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *prevEvent;

@property (nonatomic) Float64 duration;

@property (nonatomic, copy) NSDictionary* metrics;
@property (nonatomic, copy) NSDictionary* attributes;
@property (nonatomic, copy) NSDictionary* metadata;
@property (nonatomic) NSMutableDictionary *dimensions;

@property (nonatomic, copy) NSString *prevMilestone;
@property (nonatomic) BOOL isProgressQuarterSent;
@property (nonatomic) BOOL isProgressHalfSent;
@property (nonatomic) BOOL isProgressThreeQuartersSent;
@property (nonatomic) BOOL isProgressEndSent;
@property (nonatomic) BOOL isMetadataLoaded;
@property (nonatomic) BOOL isPlaybackStarted;
@property (nonatomic) BOOL isPlaybackPaused;

@property (nonatomic) NSMutableArray<NSString *> *eventCache;

+ (id)sharedInstance;
- (void)setup: (NSString *)tid;
- (void)screenView: (NSString *)screenName :(NSDictionary<NSString *, NSString *> *)customParameters;

@end

#endif /* GAUtils_h */
